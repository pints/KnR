/*
program for replacing each string of one or more blanks
 by a 0

reading from file: ex.txt

*/

#include<stdio.h>

void main(){
 FILE *f;
 char ch;
  
 //reading text from file "ex.txt"
  f = fopen("ex.txt","r+");
  while((ch=getc(f))!=EOF){
     if(ch==' ') { fseek(f,-1,SEEK_CUR);fprintf(f,"0");}
    }
 fclose(f);
}
