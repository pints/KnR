/*
fahrenheit to celsiuis conversion table upto 
20 degree f

*/
#include<stdio.h>

void main(){
 int step=20,i;
 float f=0;
 float c;
 printf("Fahrenheit\tCelsiuis\n");
 for(i=0;i<step;i++){
    c= ((5.0/9.0)*(f-32.0));
    printf("  %3.1f\t\t%3.1f\n",f,c);
    f++;
  }
}
