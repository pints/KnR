/*
 *
 *  program for writing the range of char,unsigned and signed char,long,int and short
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main(void)
{
    // range of char values
    printf("Range of char: %d to %d \n", CHAR_MIN,CHAR_MAX);
    printf("Maximum value of unsigned char: %u\n", UCHAR_MAX);
    printf("Range of signed char: %d to %d\n", SCHAR_MIN,SCHAR_MAX);
    
    // range of int values
    printf("Range of int: %d to %d\n",INT_MIN, INT_MAX);
    printf("Maximum value of unsigned int: %u\n", UINT_MAX);
    printf("Maximum value of unsigned long: %lu\n", ULONG_MAX);

    // range of long int values
    printf("Range of long int: %ld\n",LONG_MIN, LONG_MAX);
    
    // range of short
    printf("Range of short: %d to %d\n",SHRT_MIN, SHRT_MAX);
    printf("Maximum value of unsigned short: %u\n", USHRT_MAX);
    
    return 0;
}
