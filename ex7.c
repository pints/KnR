/*
program for reading text from file and write '\b' char where blank space and 
'\t' char where tab comes.

*/
#include<stdio.h>

void main(){
 FILE *f;
 char ch;
  
 //reading text from file "ex.txt"
  f = fopen("ex.txt","r+");
  while((ch=getc(f))!=EOF){
     if(ch==' ') { fseek(f,-1,SEEK_CUR);fprintf(f,"\\b");}
     if(ch=='\t') { fseek(f,-1,SEEK_CUR);fprintf(f,"\\t");}
     
    }
 fclose(f);
}
