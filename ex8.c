/*

program for printing one word at one line

*/
#include<stdio.h>

void main(){
 FILE *f;
 char ch;
  
 //reading text from file "ex.txt"
  f = fopen("ex.txt","r+");
  while((ch=getc(f))!=EOF){
     if(ch!=' ' && ch!='\t') printf("%c",ch);
     else printf("\n");
     
    }
 fclose(f);
}
