/*
 *  
 * Program which returns the first location in the line 1 where any character from the line 2 occurs
 *
 * 
 */


#include<stdio.h>

#define SIZE 200     // Maximum string length

void match_char_locator(char l1[],char l2[],int size1, int size2);  // 

void main()
{
   int i=0, j=0, s=SIZE;
   char line1[SIZE], line2[SIZE], ch;
  
   // input line 1

   printf("enter a first line: \n");
   while(i < s-1)
     {
        ch = getchar();
        if (ch == '\n')
          {
            s = 0;
          }    
        else
          {
            line1[i++] = ch;
          }
     }    
   line1[i]='\0';             // end of line1
   
   //input line 2

   printf("enter the second line: \n");
   s=SIZE;
   while(j < s-1)
     {
        ch = getchar();
        if (ch == '\n')
          {
            s = 0;
          }    
        else
          {
            line2[j++] = ch;
          }
     }    
    line2[j]='\0';           // end of line2

    match_char_locator(line1,line2,i,j); // 
}


// match_char_locator function defination

void match_char_locator(char l1[],char l2[], int size1, int size2)
{
   int k,m,n=0,location[size1],check;
  
   for(k=0; k<size1; k++)
   {
     check=0;
   	 for(m=0; m<size2; m++)
   	 {

       if(   l1[k]!=' '
       	  && l1[k]==l2[m] )
         {
       	    location[n]=m;                 // character matched in line2 so puting location of it
            n++;
            check=1;
          }
      }
       if(   l1[k]!=' '
          && check!=1
         )
         {
           location[n]=-1;                 // character not find so location = -1
           n++;
         }
   }
   
   printf("location string of line1:\n");  // new line2 after squeeze done
   for(k=0; k<n; k++)
   {
     printf("%d ",location[k]);            // first location of character in line1 where any character from the line2 occurs
   }
   printf("\n");
}
