/*
 * 
 * program for converting Hexadecimal number into Decimal number
 * 
 */

#include <stdio.h>
#include <stdlib.h>


#define SIZE 200   //maximum length of string


// check input string is valid hexadecimal number or not

int check(char temp_line[], int size)
{
    int ok=0, j;
    for(j=0; j<size; j++)
      {
        if(
                (temp_line[j]>='0' && temp_line[j]<='9') 
             || (temp_line[j]>='a' && temp_line[j]<='f') 
             || (temp_line[j]>='A' && temp_line[j]<='F')
          )
           {
             ok=1;              //input number is valid hexadecimal number
            } 
        else
           {
             ok=0;              //input number is not valid hexadecimal number so break checking further
             break;
            }
      }

  return ok;
}

// power funtion return the value of x^n

int power(int x,int n)
{
    int i; 
    int number = 1;

    for (i = 0; i < n; ++i)
      {
        number *= x;
      }

    return(number);
}

// function for hexadecimal to decimal conversion

void hex_to_decimal(char temp_line[], int size)
{
  int decimal=0, j, count=size-1, t;
  int p;
   for(j=0;j<size;j++)
    {
       if(    temp_line[j]>='a'
           && temp_line[j]<='f'
         )
        {
          t=(int)temp_line[j]-87;        // converting [a-f] into decimal value
        }
        else if(    temp_line[j]>='A'
                 && temp_line[j]<='F')
             {
                t=(int)temp_line[j]-55;  // converting [A-F] into decimal value
             }
        else
        {
          t=(int)temp_line[j]-48;        // converting ['0'-'9'] into decimal value
        }
       p=power(16,count);
       decimal+=(t*p);
       count--;
    }

   printf("your decimal number is: %d\n",decimal);
}

void main()
{
    int i=0, s=SIZE, p;
    char line[SIZE], ch;

    printf("enter hexadecimal input: \n");
    while(i < s-1)
     {
        ch = getchar();
        if (ch == '\n')
          {
            s = 0;
          }    
        else
          {
            line[i++] = ch;
          }
     }    
    line[i]='\0';  // end of line

    printf("your hexadecimal number: %s\n", line);

    p=check(line,i); 

    if(p==0)
      { 
        printf("Not Valid hexadecimal input\n"); 
      }
   else
      {
        hex_to_decimal(line,i);
      }
}

