/*
 *  
 * program that take number of bit and gives one's compliment of n number of bit, present in input bit line
 * from position p
 * 
 */


#include<stdio.h>

int bit[100], bit_size=0;

// check_bits function checks input bit is 0 OR 1

int check_bits(long int temp_line)
{
  int j,invalid=0,temp_bit[100],k;
  while(temp_line!=0)
  {
    temp_bit[bit_size]=temp_line%10;       // converting input integer into bit's and saving into temp_bit variable
    bit_size++;                            // now temp_bit have bits in reverse order from input data
    temp_line=temp_line/10;
  }
  k=bit_size-1;
  for(j=0; j<bit_size; j++)
  {
    bit[j]=temp_bit[k];                   // actual bits in correct order in bit variable
    k--;
    if(   bit[j]!=0
       && bit[j]!=1
      )
      {
        invalid=1;                      // if bit is not 0 OR 1 then invalid input
        break;
      }
    
  }

  return invalid;
}

void invert(int temp_position, int bits)
{
    int temp_bit[100],j;
    for(j=0; j<bits; j++)
    {
      temp_bit[j]=1;                       // temp_bit having all bits 1
      bit[temp_position-1]^=temp_bit[j];   // 1's compliment of bits by XOR with temp_bit
      temp_position++;
    }   
    for(j=0; j<bit_size; j++)
    {
       printf("%d ",bit[j]);              // output result bit's
    }
    printf("\n");
}

void main()
{
   int invalid_input;
   int position, num_bits;
   long int line;
   
   // input bits

   printf("enter bits: \n");
   scanf("%ld",&line);

   invalid_input=check_bits(line);  // check_bits function call, if valid input then return 0 else return 1

   if(invalid_input==1)
   {
    printf("you enterd invalid input\n");
   }
   else
   {
     printf("enter starting position of conversion:\n");   // address from conversion start to n number of bits
     scanf("%d",&position);                                // position must be between 1 to sizeof input 

     if(   position>0
        && position<=bit_size
       )
       {
          printf("enter number of bits for conversion:\n");
          scanf("%d",&num_bits);                           // number of bits which will be converted into 1's compliment

          if(num_bits<=(bit_size-position+1))              // it must be between position and sizeof input
           {
              invert(position,num_bits);
           }
          else
           {
              printf("invalid number of bits\n");
           }
       }
     else
     {
         printf("invalid position\n");
     }
   }
}