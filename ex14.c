/*
 *  
 * program that delets each character in line1 that matches any character in line2
 *
 * 
 */


#include<stdio.h>

#define SIZE 200     // Maximum string length

void squeeze(char l1[],char l2[],int size1, int size2);  // squeeze function deletes each character in line that matches in line2

void main()
{
   int i=0, j=0, s=SIZE;
   char line1[SIZE], line2[SIZE], ch;
  
   // input line 1

   printf("enter a first line: \n");
   while(i < s-1)
     {
        ch = getchar();
        if (ch == '\n')
          {
            s = 0;
          }    
        else
          {
            line1[i++] = ch;
          }
     }    
   line1[i]='\0';             // end of line1
   
   //input line 2

   printf("enter the second line: \n");
   s=SIZE;
   while(j < s-1)
     {
        ch = getchar();
        if (ch == '\n')
          {
            s = 0;
          }    
        else
          {
            line2[j++] = ch;
          }
     }    
    line2[j]='\0';           // end of line2

    squeeze(line1,line2,i,j); // squeeze function call 
}


// squeeze function defination

void squeeze(char l1[],char l2[], int size1, int size2)
{
   int k,m,n,char_del=size1;
   char temp_line[SIZE];
   for(k=0; k<size1; k++)
   {
   	 for(m=0; m<size2; m++)
   	 {
       if(   l1[k]!=' '
       	  && l1[k]==l2[m] )
       {
       	 // deleting character from line1 that matches any character in the line2

       	 for(n=k; n<size1; n++)
       	 {
       	 	l1[n]=l1[n+1];
       	 }
       	 l1[char_del]='\0';
       	 char_del--;
       }
   	 }
   }
   
   printf("After Squeeze line1 is: %s\n",l1);  // new line2 after squeeze done
}