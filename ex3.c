/*
 program for counting blank space, tab and new line char in 
 given file
 file name: ex.txt

*/
#include<stdio.h>

void main(){
  FILE *f;
  char ch;
  int bl=0,t=0,nl=0;
 //reading text from file "ex.txt"
  f = fopen("ex.txt","r");
  while((ch=getc(f))!=EOF){
     if(ch==' ') bl++;
     if(ch=='\t') t++;
     if(ch=='\n') nl++;
    }
 fclose(f);
 printf("total blanks are: %d\ntotal tabs are: %d\ntotal new line:%d\n",bl,t,nl);
}
