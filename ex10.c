/*

fahrenheit to celsiuis conversion table by conversion function

*/
#include<stdio.h>

void conversion(int s){
     int i;
     float f=0;
     float c;
     for(i=0;i<s;i++){
             c= ((5.0/9.0)*(f-32.0));
             printf("  %3.1f\t\t%3.1f\n",f,c);
             f++;
         }
}

void main(){
 int step;
 printf("how much fahrenheit you want to convert in Celsiuis? \n");
 scanf("%d",&step);  // number of fahrenheit data
 printf("Fahrenheit\tCelsiuis\n");
 conversion(step);
}
